# Demo app
to check broadcasting messages of tick time and low battery


### after 1 minute
<img src="./.images/one.png"  height="400">

### after 2 minutes
<img src="./.images/two.png"  height="400">

### after 3 minutes
<img src="./.images/three.png"  height="400">

### unsubscribed
<img src="./.images/unsubscribed.png"  height="400">