package com.example.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    lateinit var TimeTickReceiver: TimeTickReceiver
    lateinit var br: BatteryReceiver
    lateinit var textView: TextView
    var flag = true
    var passedTime = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.TimeTickReceiver = TimeTickReceiver()
        this.br = BatteryReceiver()
        this.textView = findViewById(R.id.text_view)
        this.textView.text = "время созерцания: $passedTime мин."
        findViewById<Button>(R.id.button).setOnClickListener {
            unregisterReceiver(this.TimeTickReceiver)
            Toast.makeText(this, "Unsubscribed", Toast.LENGTH_SHORT).show()
        }
            sendBroadcast(intent)

        }
    override fun onStart() {
        super.onStart()
        registerReceiver(this.TimeTickReceiver, IntentFilter("android.intent.action.TIME_TICK"))
        registerReceiver(this.br, IntentFilter("android.intent.action.BATTERY_LOW"))
    }

    internal fun addTime() {
        this.passedTime += 1
        Log.d("test", this.passedTime.toString())
        if (this.flag) {
            this.textView.text = "время созерцания: $passedTime мин."
        }

    }

    internal fun setLowBatteryTitle() {
        this.flag = false
        this.textView.text = "Накормите Ждуна, силы на исходе!"
    }



}

class TimeTickReceiver: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent) {
        (p0 as MainActivity).addTime()
    }
}

class BatteryReceiver: BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent) {
        (p0 as MainActivity).setLowBatteryTitle()
    }
}